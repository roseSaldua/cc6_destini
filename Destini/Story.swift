//
//  Story.swift
//  Destini
//
//  Created by ronne on 12/08/2019.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import Foundation

class Story {
    let storyText: String
    let answerA : String
    let answerB : String
    let nextA: Int
    let nextB: Int
    
    init (storyTxt: String, ansA: String, ansB: String, storyA: Int, storyB: Int) {
        storyText = storyTxt
        answerA = ansA
        answerB = ansB
        nextA = storyA
        nextB = storyB
    }
}
