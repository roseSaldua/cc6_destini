//
//  ViewController.swift
//  Destini
//
//  Created by Philipp Muellauer on 01/09/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    let storyFlow = StoryOutline()
    
    var storyIndex = 1
    var pickedStory = 1
    
    
    // UI Elements linked to the storyboard
    @IBOutlet weak var topButton: UIButton!         // Has TAG = 1
    @IBOutlet weak var bottomButton: UIButton!      // Has TAG = 2
    @IBOutlet weak var restartButton: UIButton!
    @IBOutlet weak var storyTextView: UILabel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        restart()
        
    }

    
    // User presses one of the buttons
    @IBAction func buttonPressed(_ sender: UIButton) {
        
            if sender.tag == 1 {
                pickedStory = storyFlow.storyList[storyIndex-1].nextA
                gotoNextPage(pickedStory)
                
            } else if sender.tag == 2 {
                pickedStory = storyFlow.storyList[storyIndex-1].nextB
                gotoNextPage(pickedStory)
            } else {
                
                print("\(sender.tag)")
            }
        print("\(pickedStory)")
    }
    
    
    func gotoNextPage(_ nextPage: Int){
        
        storyIndex = nextPage
        
        if storyIndex >= 4 && storyIndex <= 6 {
            storyTextView.text = storyFlow.storyList[storyIndex-1].storyText
            topButton.isHidden = true
            bottomButton.isHidden = true
            restartButton.isHidden = false
            restartButton.setTitle("Go Back to First Page", for: .normal)
        } else {
            storyTextView.text = storyFlow.storyList[storyIndex-1].storyText
            topButton.setTitle(storyFlow.storyList[storyIndex-1].answerA, for: .normal)
            bottomButton.setTitle(storyFlow.storyList[storyIndex-1].answerB, for: .normal)
        }
        
    }
    

    
    @IBAction func restartButtonTapped(_ sender: Any) {
        restart()
        
    }
    
    func restart() {
        
        restartButton.isHidden = true
        topButton.isHidden = false
        bottomButton.isHidden = false
        storyIndex = 1
        
        storyTextView.text = storyFlow.storyList[0].storyText
        topButton.setTitle(storyFlow.storyList[0].answerA, for: .normal)
        bottomButton.setTitle(storyFlow.storyList[0].answerB, for: .normal)
        
    }


}

